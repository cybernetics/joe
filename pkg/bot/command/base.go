/*
2019 © Postgres.ai
*/

package command

const PlanSize = 400


const SeparatorPlan = "\n[...SKIP...]\n"

const CutText = "_(The text in the preview above has been cut)_"
