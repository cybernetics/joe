module gitlab.com/postgres-ai/joe

go 1.13

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/hako/durafmt v0.0.0-20191009132224-3f39dc1ed9f4
	github.com/jessevdk/go-flags v1.4.1-0.20181221193153-c0795c8afcf4
	github.com/lib/pq v1.3.0
	github.com/mattn/go-runewidth v0.0.8 // indirect
	github.com/nlopes/slack v0.6.0
	github.com/olekukonko/tablewriter v0.0.4
	github.com/pkg/errors v0.8.1
	github.com/rs/xid v1.2.1
	github.com/sergi/go-diff v1.1.0
	github.com/sethvargo/go-password v0.1.3
	github.com/sirupsen/logrus v1.4.2
	gitlab.com/postgres-ai/database-lab v0.3.0
	gopkg.in/yaml.v2 v2.2.7
)
